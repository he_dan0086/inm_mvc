package fr.unice.polytech.mediamanager.control;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import fr.unice.polytech.mediamanager.model.Actor;
import fr.unice.polytech.mediamanager.model.Director;
import fr.unice.polytech.mediamanager.model.Film;
import fr.unice.polytech.mediamanager.model.Genre;
import fr.unice.polytech.mediamanager.model.Manager;
import fr.unice.polytech.mediamanager.model.Nationality;
import fr.unice.polytech.mediamanager.model.People;

/**
 * Cours: IHM-POO3 Polytech Nice Sohpia-SI4
 * 
 * @author Dan HE
 * 
 *         Class MyFilmControl: lien entre les views et les managers
 */
public class MyFilmControl {
	private Manager manager;
	private int nombre_Film;// comprteur des films
	private int newFilmNum = 0;// compteur des nouveaux films
	private ArrayList<Film> filmList;// tous les films
	private ArrayList<Director> dlist;// tous les directeurs
	private ArrayList<Actor> alist;// tous les acteurs
	private People newPeople = null;
	private String newPeopleName;

	/**
	 * Constructor: init the variables
	 */
	public MyFilmControl() {
		manager = Manager.createManager();
		filmList = manager.getAllFilms();
		dlist = manager.getAllDirectors();
		alist = manager.getAllActors();

	}

	/**
	 * get all detail informations of all films
	 * 
	 * @return
	 */
	public ArrayList<Map<String, Object>> getAllFilms() {
		ArrayList<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		nombre_Film = manager.getAllFilms().size();
		for (int i = 0; i < manager.getAllFilms().size(); i++)
			mapList.add(filmInfo(manager.getAllFilms().get(i)));
		return mapList;
	}

	/**
	 * get all detail informations of all directors
	 * 
	 * @return
	 */
	public ArrayList<Map<String, Object>> getAllDirectors() {
		ArrayList<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		for (Director d : dlist) {
			mapList.add(peopleInfo(d));
		}
		return mapList;
	}

	/**
	 * get all detail informations of all actors
	 * 
	 * @return
	 */
	public ArrayList<Map<String, Object>> getAllActors() {
		ArrayList<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		for (Actor d : alist) {
			mapList.add(peopleInfo(d));
		}
		return mapList;
	}

	/**
	 * get all details of a given name person
	 * 
	 * @param name
	 * @return
	 */
	public Map<String, Object> getPeopleInfo(String name) {
		ArrayList<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		mapList.addAll(getAllActors());
		mapList.addAll(getAllDirectors());
		for (Map<String, Object> m : mapList) {
			if ((m.get("FirstName") + " " + m.get("LastName"))
					.equalsIgnoreCase(name))
				return m;
		}
		return null;

	}

	/**
	 * private:get detail informations of a person it's uesd only in this class
	 * 
	 * @param a
	 *            People
	 * @return Map<String,String>
	 */
	private Map<String, Object> peopleInfo(People a) {
		Map<String, Object> actorMap = new HashMap<String, Object>();
		actorMap.put("Id", a.getId());
		actorMap.put("FirstName", a.getFirstname());
		actorMap.put("LastName", a.getLastname());
		actorMap.put("Nationality", a.getNationality().toString());
		actorMap.put("Birth", a.getBirth().getYear() + "");
		actorMap.put("Death", a.getDeath().getYear() + "");
		actorMap.put("Photo", a.getPhoto());
		return actorMap;
	}

	/**
	 * get the actors detail informations of a given title film
	 * 
	 * @param title
	 * @return
	 */
	public ArrayList<Map<String, Object>> actorsList(String title) {
		ArrayList<Film> filmsResulatList = manager.searchByTitle(title);
		ArrayList<Map<String, Object>> actorsListMap = new ArrayList<Map<String, Object>>();
		for (Film f : filmsResulatList) {
			ArrayList<Actor> actorsList = f.getActors();
			for (Actor a : actorsList)
				actorsListMap.add(peopleInfo(a));

		}
		return actorsListMap;
	}

	/**
	 * get the director detail informations of a given title film
	 * 
	 * @param title
	 * @return
	 */
	public ArrayList<Map<String, Object>> directorsList(String title) {
		ArrayList<Film> filmsList = manager.searchByTitle(title);
		ArrayList<Map<String, Object>> directorsListMap = new ArrayList<Map<String, Object>>();
		for (Film f : filmsList) {
			Director d = f.getDirector();
			directorsListMap.add(peopleInfo(d));
		}
		return directorsListMap;
	}

	/**
	 * get the detail informations of a given title film
	 * 
	 * @param title
	 * @return
	 */
	public Map<String, Object> filmInfo(String title) {
		ArrayList<Map<String, Object>> filmsList = getAllFilms();
		for (Map<String, Object> m : filmsList) {
			if (m.get("Title").toString().equalsIgnoreCase(title))
				return m;
		}
		return null;
	}

	/**
	 * get the total nomber of the films
	 * 
	 * @return
	 */
	public int getNombreFilm() {
		return nombre_Film;
	}

	/**
	 * get the detail inforamtions of the found film according to the different
	 * way
	 * 
	 * @param searchMoyen
	 * @param contenu
	 * @return
	 */
	public ArrayList<Map<String, Object>> searchFilm(String searchMoyen,
			String contenu) {
		ArrayList<Film> filmList = new ArrayList<Film>();
		ArrayList<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		switch (searchMoyen) {
		case "Title":
			filmList = manager.searchByTitle(contenu);
			break;
		case "Genre":
			for (Genre g : manager.getAllGenres()) {
				if (g.getLabelEn().equalsIgnoreCase(contenu)
						|| g.getLabelFr().equalsIgnoreCase(contenu))
					filmList = manager.searchByGenre(g);
			}
			break;

		case "Actor":
			for (Actor a : manager.getAllActors()) {
				if (a.getFirstname().equalsIgnoreCase(contenu)
						|| a.getLastname().equalsIgnoreCase(contenu))
					filmList = manager.searchByActor(a);
			}
			break;
		case "Director":
			for (Director d : manager.getAllDirectors()) {
				if (d.getFirstname().equalsIgnoreCase(contenu)
						|| d.getLastname().equalsIgnoreCase(contenu))
					filmList = manager.searchByDirector(d);
			}
			break;
		}
		for (int i = 0; i < filmList.size(); i++) {
			mapList.add(filmInfo(filmList.get(i)));
		}
		return mapList;
	}

	/**
	 * 
	 * @return list de tous les genres
	 */
	public ArrayList<String> getAllGenre() {
		ArrayList<String> genres = new ArrayList<String>();
		for (Genre g : manager.getAllGenres()) {
			genres.add(g.name());

		}
		return genres;
	}

	public ArrayList<String> getAllnationality() {
		ArrayList<String> nationalities = new ArrayList<String>();
		for (Nationality n : manager.getAllNationalities())
			nationalities.add(n.name());
		return nationalities;
	}

	/**
	 * Créer une nouvelle personne
	 * 
	 * @param p
	 * @return
	 */
	public People newPeople(Map<String, Object> p) {
		Nationality n = checkNationality(p.get("nationality"));
		newPeopleName = p.get("firstname") + " " + p.get("lastname");
		String photo = p.get("photo").toString();
		if (photo == null || photo.equals(""))
			photo = "resources/photos/emptyPhoto.jpg";
		if (p.get("genre").toString().equalsIgnoreCase("Actor")) {
			newPeople = new Actor("", p.get("firstname").toString(), p.get(
					"lastname").toString(), n, (Date) p.get("birth"),
					(Date) p.get("death"), photo);
			alist.add((Actor) newPeople);
		} else {
			newPeople = new Director("", p.get("firstname").toString(), p.get(
					"lastname").toString(), n, (Date) p.get("birth"),
					(Date) p.get("death"), photo);
			dlist.add((Director) newPeople);
		}
		return newPeople;
	}

	/**
	 * ajouter un nouveau film
	 * 
	 * @param m
	 * @return
	 */
	public boolean newFilm(Map<String, Object> m) {
		newFilmNum++;
		String title = (String) m.get("title");
		Director director = null;
		for (Director d : dlist)
			if (m.get("director").equals(
					d.getFirstname() + " " + d.getLastname()))
				director = d;
		ArrayList<Actor> actors = new ArrayList<Actor>();
		for (Object s : (ArrayList<Object>) m.get("actor"))
			for (Actor a : alist) {
				if (s.toString().equalsIgnoreCase(
						a.getFirstname() + " " + a.getLastname()))
					actors.add(a);
			}
		ArrayList<Genre> genres = new ArrayList<Genre>();
		for (String s : ((String) m.get("genre")).split(" ")) {
			Genre g = checkGenre(s);
			genres.add(g);
		}
		int runtime = Integer.parseInt((String) m.get("runtime"));
		String poster = (String) m.get("poster");
		if (poster == null || poster.equals(""))
			poster = "resources/posters/unknownPoster.jpg";
		String synopsis = (String) m.get("synopsis");
		Film f = new Film("", title, director, actors, genres, runtime, poster,
				synopsis);
		filmList.add(f);
		manager.addFilm(f);
		return false;
	}

	/**
	 * update les informations du film
	 * 
	 * @param f
	 */
	public void updateFilm(Map<String, Object> f) {
		String id = (String) f.get("Id");
		String title = (String) f.get("title");
		Director director = null;
		for (Director d : dlist)
			for (Map<String, Object> m : (ArrayList<Map<String, Object>>) f
					.get("director"))
				if ((m.get("FirstName") + " " + m.get("LastName"))
						.equalsIgnoreCase(d.getFirstname() + " "
								+ d.getLastname()))
					director = d;
		ArrayList<Actor> actors = new ArrayList<Actor>();
		for (Map<String, Object> m : (ArrayList<Map<String, Object>>) f
				.get("actor"))
			for (Actor a : alist) {
				if ((m.get("FirstName") + " " + m.get("LastName"))
						.equalsIgnoreCase(a.getFirstname() + " "
								+ a.getLastname()))
					actors.add(a);
			}
		ArrayList<Genre> genres = new ArrayList<Genre>();
		for (String s : ((String) f.get("genre")).split(" ")) {
			Genre g = checkGenre(s);
			genres.add(g);
		}
		int runtime = Integer.parseInt((String) f.get("runtime"));
		String poster = (String) f.get("poster");
		if (poster == null || poster.equals(""))
			poster = "resources/posters/unknownPoster.jpg";
		String synopsis = (String) f.get("synopsis");
		Film ff = new Film(id, title, director, actors, genres, runtime,
				poster, synopsis);
		manager.updateFilm(ff);
	}

	/**
	 * change une chaine de caractere en type de classe Nationality
	 * 
	 * @param nation
	 * @return
	 */
	public Nationality checkNationality(Object nation) {
		for (Nationality n : manager.getAllNationalities())
			if (n.getLabelEn().equalsIgnoreCase(nation.toString())
					|| n.getLabelFr().equalsIgnoreCase(nation.toString()))
				return n;
		return null;
	}

	/**
	 * changer une chaine de caractere en type de classe Genre
	 * 
	 * @param genre
	 * @return
	 */
	public Genre checkGenre(String genre) {
		for (Genre g : manager.getAllGenres())
			if (g.getLabelEn().equalsIgnoreCase(genre)
					|| g.getLabelFr().equalsIgnoreCase(genre))
				return g;
		return null;
	}

	/**
	 * tansformer un objet de Film dans un map
	 * 
	 * @param f
	 *            Film
	 * @return
	 */
	private Map<String, Object> filmInfo(Film f) {
		Map<String, Object> mapFilm = new HashMap<String, Object>();
		mapFilm.put("Id", f.getId());
		mapFilm.put("Title", f.getTitle());
		mapFilm.put("Genre", f.getGenres().toString());
		mapFilm.put("Runtime", f.getRuntime() + "");
		mapFilm.put("Poster", f.getPoster());
		mapFilm.put("Synopsis", f.getSynopsis());
		// mapFilm.put("Actors", f.getActors());
		// mapFilm.put("Director", f.getDirector());
		return mapFilm;
	}

	public String getNewPeopleName() {
		return newPeopleName;
	}

	public void deleteFilm(String id) {
		Film film = null;
		for (Film f : manager.getAllFilms())
			if (f.getId().equals(id))
				film = f;
		manager.deleteFilm(film);
	}

	public int getNewFilmNum() {
		return newFilmNum;
	}
}
