package fr.unice.polytech.mediamanager.view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

import fr.unice.polytech.mediamanager.control.MyFilmControl;

/**
 * Class EditFilmInfoFrame: la vue d'edit les informations d'un film
 * 
 * Cours:IHM-POO3
 * 
 * Polytech Nice Sohpia-SI4
 * 
 * @author Dan HE
 * 
 */
public class EditFilmInfo_View extends JFrame {

	private GridBagConstraints gridBagConstraints;
	private MyFilmControl myFilmControl;

	// menu bar
	private MenuCreator menu;

	// title
	private JLabel titleLabel;

	// poster, title, runtime, genre
	private JLabel poster;
	private JPanel panePoster;
	private JTextField changePoster;
	private JButton posteBtn;
	private LoadFile posterLoader;

	private JPanel titlePane;
	private JLabel titleFilmLabel;
	private JTextField titleFilm;

	private JPanel runtimePane;
	private JLabel runtimeLabel;
	private JTextField runtime;

	private JPanel genrePane;
	private JLabel genreLabel;
	private ArrayList<JCheckBox> genreCheck;

	// actorslist
	private JList actorsList;
	private JLabel acteur;
	private JScrollPane scollPaneActorsList;

	// directorslist
	private JLabel directorsLabel;
	private JList directorsList;
	private JScrollPane scrollPanedirectorsList;

	// infos
	private JLabel synopsisLabel;
	private JTextArea synopsis;
	private JScrollPane synopsisScroll;

	// btn
	private JPanel btnPanel;
	private JButton submitBtn;
	private JButton cancelBtn;

	public EditFilmInfo_View(Map<String, Object> mfilm) {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(FilmDetail_View.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(FilmDetail_View.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(FilmDetail_View.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(FilmDetail_View.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		setTitle(mfilm.get("Title").toString());
		setLocationRelativeTo(null);
		setVisible(true);
		setMinimumSize(new Dimension(400, 100));
		getContentPane().setLayout(new GridBagLayout());

		myFilmControl = new MyFilmControl();

		menu = new MenuCreator(this);
		titleLabelCreator();
		posterTitleCreator(mfilm);
		actorslistCreator(mfilm);
		directorsListCreator(mfilm);
		infoCreator(mfilm);
		btnCreator(mfilm);
		pack();

	}

	private void titleLabelCreator() {
		titleLabel = new JLabel("Edit of Film");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		getContentPane().add(titleLabel, gridBagConstraints);
	}

	private void posterTitleCreator(Map<String, Object> mfilm) {
		// poster
		poster = new JLabel(new ImageIcon(mfilm.get("Poster").toString()));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.gridwidth = GridBagConstraints.CENTER;
		getContentPane().add(poster, gridBagConstraints);

		panePoster = new JPanel();
		changePoster = new JTextField();
		changePoster.setText(mfilm.get("Poster").toString());
		changePoster.setColumns(20);
		posteBtn = new JButton("..");
		posteBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				posterLoader = new LoadFile(EditFilmInfo_View.this,
						changePoster, "posters");// load a picture
				poster.setIcon(new ImageIcon(changePoster.getText()));

			}
		});
		panePoster.add(changePoster);
		panePoster.add(posteBtn);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.gridwidth = GridBagConstraints.CENTER;
		getContentPane().add(panePoster, gridBagConstraints);

		// title
		titlePane = new JPanel();
		titleFilmLabel = new JLabel("Title:");
		titleFilm = new JTextField(mfilm.get("Title").toString());
		titleFilm.setColumns(20);
		titlePane.add(titleFilmLabel);
		titlePane.add(titleFilm);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(titlePane, gridBagConstraints);

		// runtime
		runtimePane = new JPanel();
		runtimeLabel = new JLabel("Runtime:");
		runtime = new JTextField(mfilm.get("Runtime").toString());
		runtimePane.add(runtimeLabel);
		runtimePane.add(runtime);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(runtimePane, gridBagConstraints);

		// genre
		genrePane = new JPanel();
		genrePane.setLayout(new GridLayout(7, 4));
		genreLabel = new JLabel("Genre:");
		genrePane.add(genreLabel);
		ArrayList<String> strings = myFilmControl.getAllGenre();
		String ss = mfilm.get("Genre").toString();
		String[] genresFilm = ss.substring(1, ss.length() - 1).split(", ");
		genreCheck = new ArrayList<JCheckBox>();
		for (int i = 0; i < strings.size(); i++) {
			JCheckBox j = new JCheckBox(strings.get(i));
			for (String s : genresFilm)
				if (s.equalsIgnoreCase(strings.get(i))) {
					j.setSelected(true);
				}
			genreCheck.add(j);
			genrePane.add(j);
		}
		getContentPane().add(genrePane);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(genrePane, gridBagConstraints);
	}

	private void actorslistCreator(Map<String, Object> mfilm) {
		acteur = new JLabel();
		acteur.setText("actorsList");

		scollPaneActorsList = new JScrollPane();
		actorsList = new JList();

		SetValeurList vl = new SetValeurList(myFilmControl.actorsList(mfilm
				.get("Title").toString()));

		actorsList.setModel(vl);
		actorsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		actorsList.addMouseListener(new ListDoubleClick("actor"));
		scollPaneActorsList.setViewportView(actorsList);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 6;
		getContentPane().add(acteur, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 0.1;
		gridBagConstraints.weighty = 0.1;
		getContentPane().add(scollPaneActorsList, gridBagConstraints);
	}

	private void directorsListCreator(Map<String, Object> mfilm) {
		directorsLabel = new JLabel();
		scrollPanedirectorsList = new JScrollPane();
		directorsList = new JList();
		SetValeurList vl = new SetValeurList(myFilmControl.directorsList(mfilm
				.get("Title").toString()));
		directorsList.setModel(vl);
		directorsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		directorsList.addMouseListener(new ListDoubleClick("director"));
		scrollPanedirectorsList.setViewportView(directorsList);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 0.1;
		gridBagConstraints.weighty = 0.1;
		getContentPane().add(scrollPanedirectorsList, gridBagConstraints);
		directorsLabel.setText("directorsList");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 6;
		getContentPane().add(directorsLabel, gridBagConstraints);

	}

	private void infoCreator(Map<String, Object> mfilm) {

		synopsisLabel = new JLabel("Synopsis:");
		synopsisScroll = new JScrollPane();
		synopsis = new JTextArea(mfilm.get("Synopsis").toString());
		synopsis.setColumns(20);
		synopsis.setLineWrap(true);
		synopsis.setRows(8);
		synopsis.setToolTipText("");
		synopsis.setWrapStyleWord(true);
		synopsisScroll.setViewportView(synopsis);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 8;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(synopsisLabel, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 9;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 0.1;
		gridBagConstraints.weighty = 0.1;
		getContentPane().add(synopsisScroll, gridBagConstraints);
	}

	private void btnCreator(final Map<String, Object> mfilm) {
		btnPanel = new JPanel();
		submitBtn = new JButton("Submit");
		submitBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Map<String, Object> m = new HashMap<String, Object>();
				m.put("Id", mfilm.get("Id"));
				m.put("title", titleFilm.getText());
				String rtime = runtime.getText();
				if (!rtime.matches("\\d+"))
					JOptionPane.showMessageDialog(null,
							"Please input an integer", "Error",
							JOptionPane.ERROR_MESSAGE);
				else {
					m.put("runtime", rtime);
					if (changePoster.getText().equals(
							mfilm.get("Poster").toString()))
						m.put("poster", mfilm.get("Poster"));
					else {
						m.put("poster", changePoster.getToolTipText());
						posterLoader.copyPhoto();
					}
					m.put("synopsis", synopsis.getText());
					String s = "";
					for (JCheckBox c : genreCheck)
						if (c.isSelected())
							s += c.getText() + " ";
					m.put("genre", s);

					ArrayList<Map<String, Object>> actors = myFilmControl
							.actorsList(mfilm.get("Title").toString());
					m.put("actor", actors);
					m.put("director", myFilmControl.directorsList(mfilm.get(
							"Title").toString()));
					myFilmControl.updateFilm(m);
					JOptionPane.showMessageDialog(null,
							"Succec to update film", "Info",
							JOptionPane.INFORMATION_MESSAGE);
					EditFilmInfo_View.this.dispose();
				}
			}
		});
		cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EditFilmInfo_View.this.dispose();
			}
		});
		btnPanel.add(submitBtn);
		btnPanel.add(cancelBtn);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 10;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		// gridBagConstraints.fill = GridBagConstraints.BOTH;
		getContentPane().add(btnPanel, gridBagConstraints);
	}

}