package fr.unice.polytech.mediamanager.view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

import fr.unice.polytech.mediamanager.control.MyFilmControl;

/**
 * Class DetailPeopleFrame: la vue des informations d'une personne
 * 
 * Cours:IHM-POO3
 * 
 * Polytech Nice Sohpia-SI4
 * 
 * @author Dan HE
 * 
 */
public class DetailPeople_View extends JFrame {

	private GridBagConstraints gridBagConstraints;
	private MyFilmControl myFilmControl;
	private String name;
	private String identify;
	// menu bar
	private MenuCreator menu;

	// title
	private JLabel title;

	// photo and name
	private JLabel photoPeople;

	private JPanel firstnamePane;
	private JPanel lastnamePane;
	private JLabel firstnameLabel;
	private JLabel firstname;
	private JLabel lastLabel;
	private JLabel lastname;

	// infos
	private JPanel nationPanel;
	private JPanel birthPanel;
	private JPanel deathPanel;
	private JLabel nationality;
	private JLabel nationalityLabel;
	private JLabel birth;
	private JLabel birthLabel;
	private JLabel deathLabel;
	private JLabel death;

	// btn
	private JPanel btnPanel;
	private JButton deleteBtn;
	private JButton editBtn;
	private JButton cancelBtn;

	public DetailPeople_View(String n) {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(DetailPeople_View.class.getName()).log(
					Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(DetailPeople_View.class.getName()).log(
					Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(DetailPeople_View.class.getName()).log(
					Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(DetailPeople_View.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		setTitle(n);
		setVisible(true);
		setLocationRelativeTo(null);
		setMinimumSize(new Dimension(400, 300));
		getContentPane().setLayout(new GridBagLayout());

		myFilmControl = new MyFilmControl();
		name = n;

		menu = new MenuCreator(this);
		titleLabelCreator();
		photonameCreator();
		infoCreator();
		btnCreator();
		pack();

	}

	private void titleLabelCreator() {
		title = new JLabel("Detail of " + identify);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		getContentPane().add(title, gridBagConstraints);
	}

	private void photonameCreator() {
		// photo
		photoPeople = new JLabel(new ImageIcon(myFilmControl
				.getPeopleInfo(name).get("Photo").toString()));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.gridwidth = GridBagConstraints.CENTER;
		gridBagConstraints.weightx = 0.1;
		gridBagConstraints.weighty = 0.1;
		getContentPane().add(photoPeople, gridBagConstraints);

		// firstname
		firstnamePane = new JPanel();
		firstnameLabel = new JLabel("Firstname:");
		firstname = new JLabel(myFilmControl.getPeopleInfo(name)
				.get("FirstName").toString());
		firstnamePane.add(firstnameLabel);
		firstnamePane.add(firstname);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.gridwidth = GridBagConstraints.CENTER;
		getContentPane().add(firstnamePane, gridBagConstraints);

		// lastname
		lastnamePane = new JPanel();
		lastLabel = new JLabel("Lastname:");
		lastname = new JLabel(myFilmControl.getPeopleInfo(name).get("LastName")
				.toString());
		lastnamePane.add(lastLabel);
		lastnamePane.add(lastname);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.gridwidth = GridBagConstraints.CENTER;
		getContentPane().add(lastnamePane, gridBagConstraints);
	}

	private void infoCreator() {
		nationPanel = new JPanel();
		nationalityLabel = new JLabel("Nationality:");
		nationality = new JLabel(myFilmControl.getPeopleInfo(name)
				.get("Nationality").toString());
		nationPanel.add(nationalityLabel);
		nationPanel.add(nationality);

		birthPanel = new JPanel();
		birthLabel = new JLabel("Birth:");
		birth = new JLabel(myFilmControl.getPeopleInfo(name).get("Birth")
				.toString());
		birthPanel.add(birthLabel);
		birthPanel.add(birth);

		deathPanel = new JPanel();
		deathLabel = new JLabel("Death:");
		String deathyear = myFilmControl.getPeopleInfo(name).get("Death")
				.toString();
		if (Integer.parseInt(deathyear) < 0)
			death = new JLabel("////");
		else
			death = new JLabel(deathyear);
		deathPanel.add(deathLabel);
		deathPanel.add(death);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		getContentPane().add(nationPanel, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		getContentPane().add(birthPanel, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		getContentPane().add(deathPanel, gridBagConstraints);
	}

	private void btnCreator() {
		btnPanel = new JPanel();
		deleteBtn = new JButton("Delete");

		editBtn = new JButton("Edit");
		cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				DetailPeople_View.this.dispose();

			}

		});
		btnPanel.add(deleteBtn);
		btnPanel.add(editBtn);
		btnPanel.add(cancelBtn);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		getContentPane().add(btnPanel, gridBagConstraints);
	}
}
