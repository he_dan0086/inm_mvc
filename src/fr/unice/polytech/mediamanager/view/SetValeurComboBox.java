package fr.unice.polytech.mediamanager.view;

import java.util.ArrayList;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
/**
 * Class SetValeurComboBox
 * 
 * Cours:IHM-POO3
 * 
 * Polytech Nice Sohpia-SI4
 * 
 * @author Dan HE, 29 mars 2015
 */
public class SetValeurComboBox extends DefaultComboBoxModel {
	ArrayList<String> strings;

	public SetValeurComboBox(ArrayList<Map<String, Object>> lm) {
		strings = new ArrayList<String>();
		for (Map<String, Object> m : lm) {
			strings.add(m.get("FirstName") + " " + m.get("LastName"));
		}
	}

	public Object getElementAt(int i) {
		return strings.get(i);
	}

	public int getSize() {
		return strings.size();
	}
}
