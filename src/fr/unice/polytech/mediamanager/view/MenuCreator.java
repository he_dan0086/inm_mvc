package fr.unice.polytech.mediamanager.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
/**
 * Class MenuCreator: créer un menu pour tous les frames
 * 
 * Cours:IHM-POO3
 * 
 * Polytech Nice Sohpia-SI4
 * 
 * @author Dan HE, 29 mars 2015
 */
public class MenuCreator {
	private JMenuBar menuBar;

	private JMenu menuActor;
	private JMenuItem addActorMenuItem;
	private JMenuItem listActorMenuItem;

	private JMenu menuMovie;
	private JMenuItem addMovieMenuItem;
	private JMenuItem listMovieMenuItem;

	private JMenu menuDirector;
	private JMenuItem addDirectorMenuItem;
	private JMenuItem listDirectorMenuItem;
	
	/**
	 * menuCreator
	 * init tous les components dans le menu
	 */
	public MenuCreator(JFrame f) {
		menuBar = new JMenuBar();
		menuActor = new JMenu("Actor");
		addActorMenuItem = new JMenuItem("Add Actor");
		menuActor.add(addActorMenuItem);
		listActorMenuItem = new JMenuItem("List Actors");
		listActorMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new List_View("Actors");

			}

		});
		menuActor.add(listActorMenuItem);
		menuBar.add(menuActor);

		menuDirector = new JMenu("Director");
		addDirectorMenuItem = new JMenuItem("Add Director");
		menuDirector.add(addDirectorMenuItem);
		listDirectorMenuItem = new JMenuItem("List Directors");
		menuDirector.add(listDirectorMenuItem);
		listDirectorMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new List_View("Directors");

			}

		});
		menuBar.add(menuDirector);

		menuMovie = new JMenu("Movie");
		addMovieMenuItem = new JMenuItem("Add Movie");
		menuMovie.add(addMovieMenuItem);
		listMovieMenuItem = new JMenuItem("List Movies");
		listMovieMenuItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new List_View("Film");
				
			}
			
		});

		menuMovie.add(listMovieMenuItem);
		menuBar.add(menuMovie);
		f.setJMenuBar(menuBar);
	}

}
