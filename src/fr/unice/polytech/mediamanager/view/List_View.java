package fr.unice.polytech.mediamanager.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

import fr.unice.polytech.mediamanager.control.MyFilmControl;

/**
 * Class ListFrame: list de tous les acteurs ou tous les directeurs
 * 
 * Cours:IHM-POO3
 * 
 * Polytech Nice Sohpia-SI4
 * 
 * @author Dan HE, 29 mars 2015
 */
public class List_View extends JFrame {
	private MyFilmControl myFilmControl;
	private String type;

	// menu bar
	private MenuCreator menu;

	// title
	private JLabel title;

	// list
	private JScrollPane scrollPaneList = new JScrollPane();
	private JList list = new JList();

	// btn
	private JPanel btnPanel;
	private JButton addBtn;
	private JButton deleteBtn;
	private JButton cancelBtn;

	public List_View(String t) {
		getContentPane().setLayout(new GridBagLayout());
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(AddFilm_View.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(AddFilm_View.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(AddFilm_View.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(AddFilm_View.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		type = t;
		myFilmControl = new MyFilmControl();
		setTitle("Lists");
		setLocationRelativeTo(null);
		setVisible(true);

		menu = new MenuCreator(this);
		titleLabelCreator();
		listCreator(t);
		btnCreator();
		pack();

	}

	private void titleLabelCreator() {
		title = new JLabel("List");
		getContentPane().setLayout(
				new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		getContentPane().add(title);
	}

	private void listCreator(String type) {
		scrollPaneList = new JScrollPane();
		list = new JList();
		if (type.equalsIgnoreCase("Actors")) {
			SetValeurList vl = new SetValeurList(myFilmControl.getAllActors());
			list.setModel(vl);
			list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			list.addMouseListener(new ListDoubleClick("actor"));
		}
		if (type.equalsIgnoreCase("Directors")) {
			SetValeurList vl = new SetValeurList(
					myFilmControl.getAllDirectors());
			list.setModel(vl);
			list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			list.addMouseListener(new ListDoubleClick("director"));
		}

		if (type.equalsIgnoreCase("film")) {
			SetValeurList vl = new SetValeurList(
					myFilmControl.getAllFilms());
			list.setModel(vl);
			list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			list.addMouseListener(new ListDoubleClick("film"));

		}

		scrollPaneList.setViewportView(list);
		getContentPane().add(scrollPaneList);

	}

	private void btnCreator() {
		btnPanel = new JPanel();
		addBtn = new JButton("Add");

		deleteBtn = new JButton("Delete");
		cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				List_View.this.dispose();

			}

		});
		btnPanel.add(addBtn);
		btnPanel.add(deleteBtn);
		btnPanel.add(cancelBtn);
		getContentPane().add(btnPanel);
	}
}
