package fr.unice.polytech.mediamanager.view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

import fr.unice.polytech.mediamanager.control.MyFilmControl;

/**
 * Class MainFrame: la vue principale, il liste tous les films, tous les
 * acteurs, et tous les directeur. Et il permet aussi de rechercher un film
 * 
 * Cours:IHM-POO3
 * 
 * Polytech Nice Sohpia-SI4
 * 
 * @author Dan HE, 29 mars 2015
 */
public class Main_View extends JFrame {

	private GridBagConstraints gridBagConstraints;
	private MyFilmControl myFilmControl;
	private ArrayList<JButton> btnList;
	private ArrayList<String> titleList;
	private ArrayList<Map<String, Object>> filmList;

	// menu bar
	private MenuCreator menu;

	// actorslist
	private JList actorsList;
	private JLabel acteur;
	private JScrollPane scollPaneActorsList;

	// directorslist
	private JLabel directorsLabel;
	private JList directorsList;
	private JScrollPane scrollPanedirectorsList;

	// search
	private JPanel searchPanel;
	private JLabel searchLabel;
	private JComboBox searchBy;
	private JTextField searchInput;
	private JButton searchBtn;

	// film list
	private JLabel movieInfoLabel;
	private JScrollPane scollPaneMoviesInfo;
	private JPanel filmPane;

	// buttons
	private JPanel btnPanel;
	private JButton addFilmBtn;
	private JButton cancelBtn;

	/**
	 * Constructeur:defini le skin
	 */
	public Main_View() {
		// utiliser Nimbus comme skin.
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(AddFilm_View.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(AddFilm_View.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(AddFilm_View.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(AddFilm_View.class.getName()).log(Level.SEVERE,
					null, ex);
		}

		myFilmControl = new MyFilmControl();
		btnList = new ArrayList<JButton>();// inti btnList
		titleList = new ArrayList<String>();
		filmList = myFilmControl.getAllFilms();
		setTitle("My Film");
		setMinimumSize(new Dimension(600, 500));
		setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setVisible(true);

		// set GridbagLayout comme moyen de layout
		getContentPane().setLayout(new GridBagLayout());
		menu = new MenuCreator(this);
		searchPanelCreator();
		actorslistCreator();
		directorsListCreator();
		filmslistCreator();
		buttnCreator();
		btnListListener();
		pack();

	}

	private void searchPanelCreator() {
		searchLabel = new JLabel("Search By:");
		searchBy = new JComboBox();
		searchBy.setModel(new DefaultComboBoxModel(new String[] { "Title",
				"Genre", "Actor", "Director" }));
		searchInput = new JTextField(20);
		searchBtn = new JButton(new ImageIcon("searchBtn.png"));
		searchBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String searchMoyen = (String) searchBy.getSelectedItem();
				String contenu = searchInput.getText();

				if (searchInput == null || searchInput.getText().equals("")) {
					filmList = myFilmControl.getAllFilms();
				} else {
					filmList = myFilmControl.searchFilm(searchMoyen, contenu);
				}
				JPanel pane = new JPanel();
				if (filmList.size() == 0) {
					JLabel label = new JLabel(new ImageIcon("no_results2.png"));
					pane.add(label);
				} else {
					btnList.clear();
					titleList.clear();
					for (int i = 0; i < filmList.size(); i++) {
						String posterPath = filmList.get(i).get("Poster")
								.toString();
						String title = filmList.get(i).get("Title").toString();
						btnList.add(new JButton(new ImageIcon(posterPath)));
						titleList.add(title);
						filmPane.hide();
						pane.add(btnList.get(i));
					}
				}
				btnListListener();
				scollPaneMoviesInfo.setViewportView(pane);
				gridBagConstraints = new GridBagConstraints();
				gridBagConstraints.gridx = 0;
				gridBagConstraints.gridy = 2;
				gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
				gridBagConstraints.fill = GridBagConstraints.BOTH;
				gridBagConstraints.weightx = 0.1;
				gridBagConstraints.weighty = 0.1;
				getContentPane().add(scollPaneMoviesInfo, gridBagConstraints);
			}

		});
		searchPanel = new JPanel();

		searchPanel.add(searchLabel);
		searchPanel.add(searchBy);
		searchPanel.add(searchInput);
		searchPanel.add(searchBtn);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		getContentPane().add(searchPanel, gridBagConstraints);
	}

	private void filmslistCreator() {
		movieInfoLabel = new JLabel();
		scollPaneMoviesInfo = new JScrollPane();
		filmPane = new JPanel();

		movieInfoLabel.setText("Movie Information:");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(movieInfoLabel, gridBagConstraints);

		for (int i = 0; i < filmList.size(); i++) {
			String posterPath = filmList.get(i).get("Poster").toString();
			String title = filmList.get(i).get("Title").toString();
			btnList.add(new JButton(new ImageIcon(posterPath)));
			titleList.add(title);
			filmPane.add(btnList.get(i));
		}

		scollPaneMoviesInfo.setViewportView(filmPane);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 0.1;
		gridBagConstraints.weighty = 0.1;
		getContentPane().add(scollPaneMoviesInfo, gridBagConstraints);
	}

	private void actorslistCreator() {
		acteur = new JLabel();
		acteur.setText("actorsList");

		scollPaneActorsList = new JScrollPane();
		actorsList = new JList();
		SetValeurList vl = new SetValeurList(myFilmControl.getAllActors());
		actorsList.setModel(vl);

		actorsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		actorsList.addMouseListener(new ListDoubleClick("actor"));
		scollPaneActorsList.setViewportView(actorsList);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		getContentPane().add(acteur, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 0.1;
		gridBagConstraints.weighty = 0.1;
		getContentPane().add(scollPaneActorsList, gridBagConstraints);
	}

	private void directorsListCreator() {
		directorsLabel = new JLabel();
		scrollPanedirectorsList = new JScrollPane();

		directorsList = new JList();
		SetValeurList vl = new SetValeurList(myFilmControl.getAllDirectors());
		directorsList.setModel(vl);

		directorsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		directorsList.addMouseListener(new ListDoubleClick("director"));
		scrollPanedirectorsList.setViewportView(directorsList);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 0.1;
		gridBagConstraints.weighty = 0.1;
		getContentPane().add(scrollPanedirectorsList, gridBagConstraints);
		directorsLabel.setText("directorsList");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 3;
		getContentPane().add(directorsLabel, gridBagConstraints);

	}

	private void buttnCreator() {
		cancelBtn = new JButton();
		addFilmBtn = new JButton();
		addFilmBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new AddFilm_View();
			}

		});
		btnPanel = new JPanel();

		addFilmBtn.setText("Add film");
		btnPanel.add(addFilmBtn);
		cancelBtn.setText("Cancel");
		cancelBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Main_View.this.dispose();
			}
		});
		btnPanel.add(cancelBtn);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		getContentPane().add(btnPanel, gridBagConstraints);
	}

	private void btnListListener() {
		for (int i = 0; i < btnList.size(); i++) {
			final String titleEnvoyer = titleList.get(i);
			btnList.get(i).addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					new FilmDetail_View(titleEnvoyer);
				}
			});
		}
	}

	public static void main(String args[]) {
		Main_View mainFrame = new Main_View();
	}
}
