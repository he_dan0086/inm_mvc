package fr.unice.polytech.mediamanager.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

import fr.unice.polytech.mediamanager.control.MyFilmControl;

/**
 * Class AddNewPeopleFrame:la vue d'ajouter une nouvelle personne
 * 
 * Cours:IHM-POO3
 * 
 * Polytech Nice Sohpia-SI4
 * 
 * @author Dan HE
 * 
 */
public class AddNewPeople_View extends JFrame {

	private GridBagConstraints gridBagConstraints;
	private LoadFile photoloader;
	private MyFilmControl myFilmControl = new MyFilmControl();
	private String[] days = new String[31];
	private String[] months = new String[12];
	private String[] years = new String[116];

	// menu bar
	private MenuCreator menu;

	// title
	private JLabel title;

	// informations principales de l'acteur
	private JLabel firstname;
	private JTextField firstnameInput;

	private JLabel lastname;
	private JTextField lastnameInput;

	private JLabel nationality;
	private JComboBox nationalityChox;

	private JLabel photo;
	private JTextField photoInput;
	private JButton photoBtn;

	// people genre:actor or director
	private JLabel peoplegenre;
	private JComboBox peoplegenreChoix;

	// Date de nassaice
	private JLabel birth;
	private JComboBox dayChoix;
	private JComboBox monthChoix;
	private JComboBox yearChoix;

	// Date de mort
	private JLabel death;
	private JComboBox daydeathChoix;
	private JComboBox monthdeathChoix;
	private JComboBox yeardeathChoix;

	// btn
	private JPanel btnPanel;
	private JButton addBtn;
	private JButton cancelBtn;

	public AddNewPeople_View(String s, MyFilmControl control) {
		getContentPane().setLayout(new GridBagLayout());
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(AddNewPeople_View.class.getName()).log(
					Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(AddNewPeople_View.class.getName()).log(
					Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(AddNewPeople_View.class.getName()).log(
					Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(AddNewPeople_View.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		setTitle("Add New People");
		setLocationRelativeTo(null);
		setVisible(true);
		myFilmControl = new MyFilmControl();
		for (int i = 0; i <= 30; i++)
			days[i] = (i + 1) + "";

		for (int i = 0; i < 12; i++)
			months[i] = (i + 1) + "";

		years[0] = "-1";
		for (int i = 1; i <= 115; i++)
			years[i] = (i + 1900) + "";

		menu = new MenuCreator(this);
		titleLabelCreator();
		actorInfosCreator();
		dateCreator();
		btnCreator(s, control);
		pack();

	}

	private void titleLabelCreator() {
		title = new JLabel("Add People");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		getContentPane().add(title, gridBagConstraints);
	}

	private void actorInfosCreator() {

		// firstname
		firstname = new JLabel("Firstname:	");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(firstname, gridBagConstraints);

		firstnameInput = new JTextField();
		firstnameInput.setColumns(20);
		firstnameInput.setToolTipText("");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		getContentPane().add(firstnameInput, gridBagConstraints);

		// lastname
		lastname = new JLabel("Lastname:	");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(lastname, gridBagConstraints);

		lastnameInput = new JTextField();
		lastnameInput.setColumns(20);
		lastname.setToolTipText("");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		getContentPane().add(lastnameInput, gridBagConstraints);

		// nationality
		nationality = new JLabel("Nationality:	");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(nationality, gridBagConstraints);

		nationalityChox = new JComboBox();

		nationalityChox.setModel(new DefaultComboBoxModel() {
			ArrayList<String> strings = myFilmControl.getAllnationality();

			public Object getElementAt(int i) {
				return strings.get(i);
			}

			public int getSize() {
				return strings.size();
			}
		});
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		getContentPane().add(nationalityChox, gridBagConstraints);

		// people genre:director or actor

		// photo
		photo = new JLabel("Photo:	");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(photo, gridBagConstraints);

		photoInput = new JTextField();
		photoInput.setColumns(15);
		photoInput.setToolTipText("");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		// gridBagConstraints.fill = GridBagConstraints.BOTH;
		getContentPane().add(photoInput, gridBagConstraints);

		photoBtn = new JButton("..");
		photoBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				photoloader = new LoadFile(AddNewPeople_View.this, photoInput,
						"photos");// load a picture

			}
		});
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 5;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		// gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		getContentPane().add(photoBtn, gridBagConstraints);

		// genre de la personne
		peoplegenre = new JLabel("Genre of people:");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(peoplegenre, gridBagConstraints);

		peoplegenreChoix = new JComboBox();
		peoplegenreChoix.setModel(new DefaultComboBoxModel(new String[] {
				"Actor", "Director" }));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(peoplegenreChoix, gridBagConstraints);

	}

	private void dateCreator() {

		// birth
		birth = new JLabel("Birth:");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(birth, gridBagConstraints);

		dayChoix = new JComboBox();
		dayChoix.setModel(new DefaultComboBoxModel(days));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(dayChoix, gridBagConstraints);
		monthChoix = new JComboBox();
		monthChoix.setModel(new DefaultComboBoxModel(months));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(monthChoix, gridBagConstraints);
		yearChoix = new JComboBox();
		yearChoix.setModel(new DefaultComboBoxModel(years));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 3;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(yearChoix, gridBagConstraints);

		// death
		death = new JLabel("Death:");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(death, gridBagConstraints);

		daydeathChoix = new JComboBox();
		daydeathChoix.setModel(new DefaultComboBoxModel(days));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(daydeathChoix, gridBagConstraints);
		monthdeathChoix = new JComboBox();
		monthdeathChoix.setModel(new DefaultComboBoxModel(months));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(monthdeathChoix, gridBagConstraints);
		yeardeathChoix = new JComboBox();
		yeardeathChoix.setModel(new DefaultComboBoxModel(years));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 3;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(yeardeathChoix, gridBagConstraints);

	}

	private void btnCreator(final String g, final MyFilmControl control) {
		btnPanel = new JPanel();
		addBtn = new JButton("Add");
		addBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Map<String, Object> person = new HashMap<String, Object>();
				person.put("firstname", firstnameInput.getText());
				person.put("lastname", lastnameInput.getText());
				person.put("nationality",
						(String) nationalityChox.getSelectedItem());
				String photo = photoInput.getToolTipText();
				person.put("photo", photo);
				// add photo
				photoloader.copyPhoto();
				person.put("genre", peoplegenreChoix.getSelectedItem()
						.toString());
				int year = Integer.parseInt(yearChoix.getSelectedItem()
						.toString());
				Date birth = new Date(year, 0, 0);
				person.put("birth", birth);
				int yeardeath = Integer.parseInt(yeardeathChoix
						.getSelectedItem().toString());
				Date death = new Date(yeardeath, 0, 0);
				person.put("death", death);
				control.newPeople(person);
				if (g.equalsIgnoreCase("Actor")) {
					int i = AddFilm_View.indexActor;
					AddFilm_View.actorsChoixList.get(i - 1).setModel(
							new DefaultComboBoxModel(
									new String[] { firstnameInput.getText()
											+ " " + lastnameInput.getText() }));
				} else if (g.equalsIgnoreCase("Director"))
					AddFilm_View.directorChoix
							.setModel(new DefaultComboBoxModel(
									new String[] { firstnameInput.getText()
											+ " " + lastnameInput.getText() }));

				AddNewPeople_View.this.dispose();
			}

		});
		cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AddNewPeople_View.this.dispose();
			}
		});
		btnPanel.add(addBtn);
		btnPanel.add(cancelBtn);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 8;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		getContentPane().add(btnPanel, gridBagConstraints);
	}
}
