package fr.unice.polytech.mediamanager.view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.swing.AbstractListModel;

import fr.unice.polytech.mediamanager.control.MyFilmControl;

/**
 * Class SetValeurList: pour choisir une image
 * 
 * Cours:IHM-POO3
 * 
 * Polytech Nice Sohpia-SI4
 * 
 * @author Dan HE, 29 mars 2015
 */
public class SetValeurList extends AbstractListModel {
	ArrayList<String> strings;

	SetValeurList(ArrayList<Map<String, Object>> lm) {
		strings = new ArrayList<String>();

		for (Map<String, Object> m : lm) {
			Iterator keys = m.keySet().iterator();
			while (keys.hasNext()) {
				String key = (String) keys.next();
				if ("FirstName".equals(key))
					strings.add(m.get("FirstName").toString() + " "
							+ m.get("LastName"));
				else if ("Title".equals(key))
					strings.add(m.get("Title").toString());
			}
		}
	}

	@Override
	public Object getElementAt(int i) {
		return strings.get(i);
	}

	@Override
	public int getSize() {
		return strings.size();
	}
}
