package fr.unice.polytech.mediamanager.view;

import java.awt.FileDialog;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

import fr.unice.polytech.mediamanager.control.MyFilmControl;

/**
 * Class AddFilmFrame :la vue d'ajouter un film
 * 
 * Cours:IHM-POO3
 * 
 * Polytech Nice Sohpia-SI4
 * 
 * @author Dan HE
 * 
 */
public class AddFilm_View extends JFrame {
	private String posterPath;
	private MyFilmControl myFilmControl;
	private LoadFile posterLoader;
	static int indexActor = 1;
	static ArrayList<JComboBox> actorsChoixList;

	// menu bar
	private MenuCreator menu;
	// title
	private JLabel title;

	// informations principales du film
	private JPanel titlePane;
	private JLabel titleFilm;
	private JTextField titleInput;

	private JPanel runtimPane;
	private JLabel runtime;
	private JTextField runtimInput;

	private JPanel genrePane;
	private JLabel genre;
	private ArrayList<JCheckBox> genreCheck;
	// private JComboBox genreChoix;

	private JPanel panePoster;
	private JLabel poster;
	private JTextField posterInput;
	private JButton fileBtn;

	private JPanel synopsisPane;
	private JLabel sysnopsis;
	private JTextArea sysnopsisInput;
	private JScrollPane scollPaneSynopsis;

	// people informations du film
	private JScrollPane actorsScrol;
	private JPanel actorsPane;
	private JPanel actorDefaut;
	private JLabel actor;
	private JComboBox actorChoix;
	private JButton addActorBtn;
	private JButton newActorBtn;

	private JPanel directorsPane;
	private JPanel directorDefaut;
	private JLabel director;
	static JComboBox directorChoix;
	private JButton addDirectorBtn;
	private JButton newDirectorBtn;

	// btn
	private JPanel btnPanel;
	private JButton submitBtn;
	private JButton cancelBtn;

	public AddFilm_View() {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(AddFilm_View.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(AddFilm_View.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(AddFilm_View.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(AddFilm_View.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		getContentPane().setLayout(
				new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		setTitle("Add Film");
		setLocationRelativeTo(null);
		setVisible(true);
		myFilmControl = new MyFilmControl();
		actorsChoixList = new ArrayList<JComboBox>();

		menu = new MenuCreator(this);
		titleLabelCreator();
		moviesInfoPrincipalCreator();
		peopleInfoCreator();
		btnCreator();
		pack();

	}

	private void titleLabelCreator() {
		title = new JLabel("Add Film");
		getContentPane().add(title);
	}

	private void moviesInfoPrincipalCreator() {
		// title
		titlePane = new JPanel();
		titleFilm = new JLabel("Title:");
		titleInput = new JTextField();
		titleInput.setColumns(20);
		titlePane.add(titleFilm);
		titlePane.add(titleInput);
		getContentPane().add(titlePane);

		// runtime
		runtimPane = new JPanel();
		runtime = new JLabel("RunTime:	");
		runtimInput = new JTextField();
		runtimInput.setColumns(20);
		runtimInput.setToolTipText("");
		runtimPane.add(runtime);
		runtimPane.add(runtimInput);
		getContentPane().add(runtimPane);

		// poster
		panePoster = new JPanel();
		poster = new JLabel("Poster:	");
		posterInput = new JTextField();
		posterInput.setColumns(16);
		posterInput.setToolTipText("");
		fileBtn = new JButton("..");
		fileBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				posterLoader = new LoadFile(AddFilm_View.this, posterInput,
						"posters");// load a picture

			}
		});
		posterInput.setText(posterPath);
		panePoster.add(poster);
		panePoster.add(posterInput);
		panePoster.add(fileBtn);
		getContentPane().add(panePoster);

		// genre
		genrePane = new JPanel();
		genrePane.setLayout(new GridLayout(7, 4));
		genre = new JLabel("Genre:");
		genrePane.add(genre);
		ArrayList<String> strings = myFilmControl.getAllGenre();
		genreCheck = new ArrayList<JCheckBox>();
		for (int i = 0; i < strings.size(); i++) {
			JCheckBox j = new JCheckBox(strings.get(i));
			genreCheck.add(j);
			genrePane.add(j);
		}
		getContentPane().add(genrePane);

		// synopsis
		synopsisPane = new JPanel();
		sysnopsis = new JLabel("Synopsis:");
		sysnopsisInput = new JTextArea();
		scollPaneSynopsis = new JScrollPane();

		sysnopsisInput.setColumns(20);
		sysnopsisInput.setLineWrap(true);
		sysnopsisInput.setRows(8);
		sysnopsisInput.setToolTipText("");
		sysnopsisInput.setWrapStyleWord(true);
		scollPaneSynopsis.setViewportView(sysnopsisInput);
		synopsisPane.add(sysnopsis);
		synopsisPane.add(scollPaneSynopsis);
		getContentPane().add(synopsisPane);
	}

	private void peopleInfoCreator() {

		// Actor
		actorsPane = new JPanel();
		actorsPane.setLayout(new BoxLayout(actorsPane, BoxLayout.Y_AXIS));
		actorDefaut = new JPanel();
		actor = new JLabel("Actor:");
		actorChoix = new JComboBox();
		final SetValeurComboBox choixActors = new SetValeurComboBox(
				myFilmControl.getAllActors());
		actorsChoixList.add(actorChoix);
		actorChoix.setModel(choixActors);
		newActorBtn = new JButton("New Actor");
		newActorBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new AddNewPeople_View("actor", myFilmControl);

			}
		});
		addActorBtn = new JButton("+");
		addActorBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				newActorPane();
				indexActor++;
			}
		});
		actorDefaut.add(actor);
		actorDefaut.add(actorChoix);
		actorDefaut.add(newActorBtn);
		actorDefaut.add(addActorBtn);
		actorsPane.add(actorDefaut);
		// actorsScrol.add(actorsPane);
		getContentPane().add(actorsPane);

		// director
		directorsPane = new JPanel();
		directorsPane.setLayout(new BoxLayout(directorsPane, BoxLayout.Y_AXIS));
		directorDefaut = new JPanel();
		director = new JLabel("Director:");
		directorChoix = new JComboBox();
		SetValeurComboBox directorsChoix = new SetValeurComboBox(
				myFilmControl.getAllDirectors());
		directorChoix.setModel(directorsChoix);

		newDirectorBtn = new JButton("New Director");
		newDirectorBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new AddNewPeople_View("Director", myFilmControl);
			}
		});
		directorDefaut.add(director);
		directorDefaut.add(directorChoix);
		directorDefaut.add(newDirectorBtn);
		directorsPane.add(directorDefaut);
		getContentPane().add(directorsPane);
	}

	private void newActorPane() {
		JPanel actorPane1 = new JPanel();

		JLabel actor1 = new JLabel("Actor:");
		JComboBox actorChoix1 = new JComboBox();
		SetValeurComboBox actorsChoix1 = new SetValeurComboBox(
				myFilmControl.getAllActors());
		actorChoix1.setModel(actorsChoix1);
		actorsChoixList.add(actorChoix1);
		JButton newBtn = new JButton("New Actor");
		newBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new AddNewPeople_View("actor", myFilmControl);
			}

		});

		actorPane1.add(actor1);
		actorPane1.add(actorChoix1);
		actorPane1.add(newBtn);
		actorsPane.add(actorPane1);
	}

	private void btnCreator() {
		btnPanel = new JPanel();
		submitBtn = new JButton("Submit");
		submitBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Map<String, Object> m = new HashMap<String, Object>();
				m.put("title", titleInput.getText());
				String runtime = runtimInput.getText();
				if (!runtime.matches("\\d+"))
					JOptionPane.showMessageDialog(null,
							"Please input an integer", "Error",
							JOptionPane.ERROR_MESSAGE);
				else {
					m.put("runtime", runtime);
					m.put("poster", posterInput.getToolTipText());
					posterLoader.copyPhoto();
					m.put("synopsis", sysnopsisInput.getText());
					String s = "";
					for (JCheckBox c : genreCheck)
						if (c.isSelected())
							s += c.getText() + " ";
					m.put("genre", s);
					ArrayList<Object> actors = new ArrayList<Object>();
					for (JComboBox c : actorsChoixList) {
						actors.add(c.getSelectedItem());
					}
					m.put("actor", actors);
					m.put("director", directorChoix.getSelectedItem());
					myFilmControl.newFilm(m);
					JOptionPane.showMessageDialog(null, "Succec to add film",
							"Info", JOptionPane.INFORMATION_MESSAGE);
					AddFilm_View.this.dispose();
				}
			}

		});
		cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				AddFilm_View.this.dispose();

			}

		});
		btnPanel.add(submitBtn);
		btnPanel.add(cancelBtn);
		getContentPane().add(btnPanel);
	}
}
