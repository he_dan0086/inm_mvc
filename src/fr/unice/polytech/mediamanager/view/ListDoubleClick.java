package fr.unice.polytech.mediamanager.view;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JList;

/**
 * Class ListDoubleClick: l'action de deux click sur un item d'une liste
 * 
 * Cours:IHM-POO3
 * 
 * Polytech Nice Sohpia-SI4
 * 
 * @author Dan HE, 29 mars 2015
 */
public class ListDoubleClick extends MouseAdapter {
	String _type;
	public ListDoubleClick(String type){
		_type=type;
		
	}
	public void mouseClicked(MouseEvent evt) {

		if (evt.getClickCount() == 2) {
			if (_type.equalsIgnoreCase("actor")
					|| _type.equalsIgnoreCase("director"))
				new DetailPeople_View(((JList) evt.getSource())
						.getSelectedValue().toString());
			else if(_type.equalsIgnoreCase("film"))
				new FilmDetail_View(((JList) evt.getSource())
						.getSelectedValue().toString());
		}
	}
}
