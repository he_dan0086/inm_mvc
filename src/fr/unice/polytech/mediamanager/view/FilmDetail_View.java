package fr.unice.polytech.mediamanager.view;

import java.awt.Dimension;
import java.awt.Event;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

import fr.unice.polytech.mediamanager.control.MyFilmControl;
/**
 * Class FilmDetailFrame: la vue des informations d'un film
 * 
 * Cours:IHM-POO3
 * 
 * Polytech Nice Sohpia-SI4
 * 
 * @author Dan HE, 29 mars 2015
 */
public class FilmDetail_View extends JFrame {

	private GridBagConstraints gridBagConstraints;
	private MyFilmControl myFilmControl;
	private Map<String, Object> film;

	// menu bar
	private MenuCreator menu;

	// title
	private JLabel titleLabel;

	// poster, title, runtime, genre
	private JLabel poster;

	private JPanel titlePane;
	private JLabel titleFilmLabel;
	private JLabel titleFilm;

	private JPanel runtimePane;
	private JLabel runtimeLabel;
	private JLabel runtime;

	private JPanel genrePane;
	private JLabel genreLabel;
	private JLabel genre;

	// actorslist
	private JList actorsList;
	private JLabel acteur;
	private JScrollPane scollPaneActorsList;

	// directorslist
	private JLabel directorsLabel;
	private JList directorsList;
	private JScrollPane scrollPanedirectorsList;

	// infos
	private JLabel synopsisLabel;
	private JTextArea synopsis;
	private JScrollPane synopsisScroll;

	// btn
	private JPanel btnPanel;
	private JButton deleteBtn;
	private JButton editBtn;
	private JButton cancelBtn;

	public FilmDetail_View(String title) {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(FilmDetail_View.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(FilmDetail_View.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(FilmDetail_View.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(FilmDetail_View.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		setTitle(title);
		setLocationRelativeTo(null);
		setVisible(true);
		setMinimumSize(new Dimension(400, 100));
		getContentPane().setLayout(new GridBagLayout());

		myFilmControl = new MyFilmControl();
		film = myFilmControl.filmInfo(title);

		menu = new MenuCreator(this);
		titleLabelCreator();
		posterTitleCreator();
		actorslistCreator();
		directorsListCreator();
		infoCreator();
		btnCreator();
		pack();

	}

	private void titleLabelCreator() {
		titleLabel = new JLabel("Detail of Film");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		getContentPane().add(titleLabel, gridBagConstraints);
	}

	private void posterTitleCreator() {
		// photo
		poster = new JLabel(new ImageIcon(film.get("Poster").toString()));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.gridwidth = GridBagConstraints.CENTER;
		// gridBagConstraints.weightx = 0.1;
		// gridBagConstraints.weighty = 0.1;
		getContentPane().add(poster, gridBagConstraints);

		// title
		titlePane = new JPanel();
		titleFilmLabel = new JLabel("Title:");
		titleFilm = new JLabel(film.get("Title").toString());
		titlePane.add(titleFilmLabel);
		titlePane.add(titleFilm);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(titlePane, gridBagConstraints);

		// runtime
		runtimePane = new JPanel();
		runtimeLabel = new JLabel("Runtime:");
		runtime = new JLabel(film.get("Runtime").toString());
		runtimePane.add(runtimeLabel);
		runtimePane.add(runtime);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(runtimePane, gridBagConstraints);

		// genre
		genrePane = new JPanel();
		genreLabel = new JLabel("Genre:");
		genre = new JLabel(film.get("Genre").toString());
		genrePane.add(genreLabel);
		genrePane.add(genre);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(genrePane, gridBagConstraints);
	}

	private void actorslistCreator() {
		acteur = new JLabel();
		acteur.setText("actorsList");

		scollPaneActorsList = new JScrollPane();
		actorsList = new JList();

		SetValeurList vl = new SetValeurList(myFilmControl.actorsList(film.get(
				"Title").toString()));

		actorsList.setModel(vl);
		actorsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		actorsList.addMouseListener(new ListDoubleClick("actor"));
		scollPaneActorsList.setViewportView(actorsList);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		getContentPane().add(acteur, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 0.1;
		gridBagConstraints.weighty = 0.1;
		getContentPane().add(scollPaneActorsList, gridBagConstraints);
	}

	private void directorsListCreator() {
		directorsLabel = new JLabel();
		scrollPanedirectorsList = new JScrollPane();
		directorsList = new JList();
		SetValeurList vl = new SetValeurList(myFilmControl.directorsList(film
				.get("Title").toString()));
		directorsList.setModel(vl);
		directorsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		directorsList.addMouseListener(new ListDoubleClick("director"));
		scrollPanedirectorsList.setViewportView(directorsList);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		getContentPane().add(scrollPanedirectorsList, gridBagConstraints);
		directorsLabel.setText("directorsList");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 5;
		getContentPane().add(directorsLabel, gridBagConstraints);

	}

	private void infoCreator() {

		synopsisLabel = new JLabel("Synopsis:");
		synopsisScroll = new JScrollPane();
		synopsis = new JTextArea(film.get("Synopsis").toString());
		synopsis.setColumns(20);
		synopsis.setLineWrap(true);
		synopsis.setRows(8);
		synopsis.setToolTipText("");
		synopsis.setWrapStyleWord(true);
		synopsis.setEditable(false);
		synopsisScroll.setViewportView(synopsis);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		getContentPane().add(synopsisLabel, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 8;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 0.1;
		gridBagConstraints.weighty = 0.1;
		getContentPane().add(synopsisScroll, gridBagConstraints);
	}

	private void btnCreator() {
		btnPanel = new JPanel();
		deleteBtn = new JButton("Delete");
		deleteBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Are you sure to delete?",
						"Warning", JOptionPane.WARNING_MESSAGE);
				myFilmControl.deleteFilm(film.get("Id").toString());
				FilmDetail_View.this.dispose();

			}

		});
		editBtn = new JButton("Edit");
		editBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new EditFilmInfo_View(film);
			}

		});
		cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				FilmDetail_View.this.dispose();
			}
		});
		btnPanel.add(deleteBtn);
		btnPanel.add(editBtn);
		btnPanel.add(cancelBtn);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 9;
		gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
		// gridBagConstraints.fill = GridBagConstraints.BOTH;
		getContentPane().add(btnPanel, gridBagConstraints);
	}
}
