package fr.unice.polytech.mediamanager.view;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

/**
 * Class LoadFile: pour choisir une image
 * 
 * Cours:IHM-POO3
 * 
 * Polytech Nice Sohpia-SI4
 * 
 * @author Dan HE, 29 mars 2015
 */
public class LoadFile {
	final String[] extensions = new String[] { ".jpg", ".bmp", ".png" };
	JFileChooser fc;
	JFrame frame;
	JTextField textField;
	String folderName;
	File dest;

	public LoadFile(JFrame f, JTextField t, String fn) {
		fc = new JFileChooser();
		frame = f;
		textField = t;
		folderName = fn;
		loadPhoto(folderName);

	}

	private boolean loadPhoto(String folderName) {

		fc.setFileFilter(new FileFilter() {
			@Override
			public boolean accept(File f) {
				if (f.isDirectory()) {
					return true;
				}
				for (String s : extensions) {
					if (f.getName().endsWith(s)) {
						return true;
					}
				}
				return false;
			}

			@Override
			public String getDescription() {
				String result = "";
				for (int i = 0; i < extensions.length; i++) {
					result += "*" + extensions[i];
					if (i != extensions.length - 1) {
						result += ", ";
					}
				}
				return result;
			}
		});

		int returnVal = fc.showOpenDialog(frame);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			fc.getSelectedFile();
			String name = fc.getSelectedFile().getName();
			dest = new File("resources/" + folderName + "/" + name);
			textField.setText(fc.getSelectedFile().toPath().toString());
			textField.setToolTipText(dest.toPath().toString());
			textField.setEditable(false);
			return true;
		} else {
			return false;
		}
	}

	public void copyPhoto() {
		if (!Files.exists(dest.toPath(), LinkOption.NOFOLLOW_LINKS)) {
			try {
				Files.copy(fc.getSelectedFile().toPath(), dest.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}